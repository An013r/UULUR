def parsepkg(file):
    fl = open(file, "r")
    for line in fl.readlines():
        print(line[0:11], line[12:])
        if line[0:7] == "pkgname":
            name = line[8:].replace("\n", "")
        elif line[0:6] == "pkgver":
            ver = line[7:].replace("\n", "")
        elif line[0:4] == "arch":
            arch = line[5:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        elif line[0:7] == "pkgdesc":
            desc = line[8:].replace("\n", "").replace("\"", "")
        elif line[0:7] == "license":
            lic = line[8:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        elif line[0:7] == "depends" and line[0:8] != "depends_":
            depends = line[8:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        elif line[0:12] == "depends_fbsd":

            fbsd = line[13:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        elif line[0:12] == "depends_obsd":
            obsd = line[13:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        elif line[0:13] == "depends_linux":
            linux = line[14:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        elif line[0:13] == "depends_macos":
            macos = line[14:].replace("(", "").replace(")", "").replace(" ", "").replace("\n", "").replace("\"", "").split(",")
        
    return name, desc, ver, arch, lic, depends, linux, fbsd, obsd, macos

